
# coding: utf-8

# In[ ]:


import shuffle_numbers
def normalize(data,newmax,newmin):
    ma = max(data)
    mi = min(data)
    l2 = [((i-mi)/(ma-mi))*(newmax-newmin)+newmin for i in data]
    return l2

